const express = require('express');
const port = process.env.PORT || 3000;
const app = express();

app.get('/api/exemplo', (req, res) => {
    const auth = req.get('authorization');
    if(!auth) {
        res.set("WWW-Authenticate", "Basic realm=\"Autorização requerida\"");
        return res.status(401).send('Autorização requerida');
    }else{
        const credentials = new Buffer(auth.split(" ").pop(), "base64").toString("ascii").split(":");

        if(credentials[0] === "everton" && credentials[1] === "senha"){
            return res.send("Acesso garantido!");
        }else{
            return res.status(401).send('Autorização requerida (Crendenciais inválidas)');
        }
    }
});

app.listen(port, () => {
    console.log("Servidor Iniciado");
});